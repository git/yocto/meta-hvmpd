do_deploy:append() {
    if [ "${SOUND_CARD}" = "iqaudiodac" ]; then
        # Disable Pi’s on-board sound card (snd_bcm2835):
        sed -i 's/^#\(dtparam=audio=\).*/\1off/' $CONFIG

        # Enable IQaudio sound card:
        echo "dtoverlay=iqaudio-dacplus" >> $CONFIG

        # Enable I2S mode:
        sed -i 's/^#\(dtparam=i2s=\).*/\1on/' $CONFIG
    else
        # Enable Pi’s on-board sound card (load snd_bcm2835):
        sed -i 's/^#\(dtparam=audio=\).*/\1on/' $CONFIG
    fi

    # Pretends all audio formats are unsupported by display. This
    # forces ALSA to use analogue output:
    sed -i 's/^#\(hdmi_ignore_edid_audio=\).*/\11/' $CONFIG

    # Prevent any HDMI connection from transmitting sound output:
    sed -i 's/^\(.*dtoverlay=vc4-kms-v3d\).*/\1,noaudio/' $CONFIG
}
