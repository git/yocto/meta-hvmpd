CMDLINE_COMPAT_ALSA ?= "snd_bcm2835.enable_compat_alsa=1"

CMDLINE_HEADPHONES ?= "snd_bcm2835.enable_headphones=1"

CMDLINE += " \
    ${CMDLINE_COMPAT_ALSA} \
    ${CMDLINE_HEADPHONES} \
"
