FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

do_compile:prepend() {
    sed -i -e 's/@@ROOT_PART_A_ID@@/${ROOT_PART_A_ID}/' \
        "${WORKDIR}/boot.cmd.in"
}
