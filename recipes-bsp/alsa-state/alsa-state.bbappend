FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
     file://asound.state \
"

do_install:append() {
    install -d ${D}${sysconfdir}
    echo "defaults.pcm.card ${ALSA_DEVICE_ID}" >> ${D}${sysconfdir}/asound.conf
    echo "defaults.ctl.card ${ALSA_DEVICE_ID}" >> ${D}${sysconfdir}/asound.conf
}
