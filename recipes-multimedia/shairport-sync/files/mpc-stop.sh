#!/bin/sh

# This script is used by shairport-sync to stop MPD
# before starting a new playback session, so that it can
# access the sound card.

mpc stop
