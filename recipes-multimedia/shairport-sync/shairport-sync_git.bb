DESCRIPTION = "AirPlay audio player. Shairport Sync adds multi-room capability with Audio Synchronisation"
LICENSE="MIT"
LIC_FILES_CHKSUM = "file://LICENSES;md5=9f329b7b34fcd334fb1f8e2eb03d33ff"

SRCBRANCH = "master"
SRCREV = "${AUTOREV}"
SRC_URI = "git://github.com/mikebrady/shairport-sync;protocol=https;branch=${SRCBRANCH}"

SRC_URI += " \
    file://shairport-sync.conf.in \
    file://mpc-stop.sh \
"

S = "${WORKDIR}/git"

EXTRA_OECONF = " \
    --with-alsa \
    --with-stdout \
    --with-ssl=openssl \
    --with-avahi \
    --with-metadata \
    --with-libdaemon \
    --with-systemd \
    --without-create-user-group \
"

DEPENDS = " libconfig popt avahi openssl alsa-lib"

inherit autotools pkgconfig systemd

AIRPLAY_SERVER_NAME ?= "%h shairplay-sync"

do_install:append() {
    sed -e "s/@AIRPLAY_SERVER_NAME@/${AIRPLAY_SERVER_NAME}/" \
        ${WORKDIR}/shairport-sync.conf.in > ${WORKDIR}/shairport-sync.conf
    install -d ${D}${sysconfdir}/
    install -m 0644 ${WORKDIR}/shairport-sync.conf ${D}${sysconfdir}/

    # Modify shairport-sync start arguments to always stop MPD before playing:
    sed -i -e "s@^\(ExecStart=.*\)@\1 -w --on-start=${bindir}/mpc-stop.sh@g" \
        ${D}${systemd_unitdir}/system/shairport-sync.service

    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/mpc-stop.sh ${D}${bindir}
}

SYSTEMD_SERVICE:${PN} = " \
     shairport-sync.service \
"

inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = " \
    --system \
    --no-create-home \
    --home ${runstatedir}/shairport-sync \
    --groups audio \
    --user-group shairport-sync \
"
