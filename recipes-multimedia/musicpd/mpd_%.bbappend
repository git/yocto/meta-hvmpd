FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
     file://radios.m3u \
     file://fip.m3u \
     file://jazz.m3u \
     file://classique.m3u \
"

SOUND_CARD ?= "default"

do_install:append() {
    ALSA_MIXER="PCM"
}

do_install:append:wandboard() {
    ALSA_DEVICE_NAME="imx6wandboardsg"
}

do_install:append:rpi() {
    if [ "${SOUND_CARD}" = "iqaudiodac" ]; then
        ALSA_DEVICE_NAME="IQaudIODAC"
        ALSA_MIXER="Digital"
    else
        # Internal sound card
        ALSA_DEVICE_NAME="headphones"
    fi
}

do_install:append() {
    if [ "${MPD_MUSIC_DIR}" != "" ]; then
        sed -i -e 's:^\(music_directory\).*:\1 \"${MPD_MUSIC_DIR}\":g' \
	    ${D}/${sysconfdir}/mpd.conf
    fi

    echo "zeroconf_enabled    \"yes\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "zeroconf_name       \"Serveur Musique MPD\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "audio_output {" >> ${D}/${sysconfdir}/mpd.conf
    echo "    type    \"alsa\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "    name    \"${ALSA_DEVICE_NAME}\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "    device  \"hw:${ALSA_DEVICE_ID},0\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "    mixer_control \"${ALSA_MIXER}\"" >> ${D}/${sysconfdir}/mpd.conf
    echo "}" >> ${D}/${sysconfdir}/mpd.conf

    install -m 644 -o mpd -g audio ${WORKDIR}/*.m3u ${D}/${localstatedir}/lib/mpd/playlists/

    # Disabling autostart of mpd.socket, in order to enable zeroconf:
    rm ${D}${systemd_unitdir}/system/mpd.socket
}

# Disabling autostart of mpd.socket, in order to enable zeroconf:
SYSTEMD_SERVICE:${PN} = "mpd.service"
