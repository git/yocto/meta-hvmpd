DESCRIPTION = "MPD Web GUI - written in C, utilizing Websockets and Bootstrap/JS"
SECTION = "multimedia"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=eb723b61539feef013de476e68b5c50a"

PV = "1.2.3+git${SRCPV}"

DEPENDS += "libmpdclient openssl"

SRCBRANCH = "hvmpd"
SRCREV = "${AUTOREV}"

# Original URI: git://github.com/SuperBFG7/ympd.git
SRC_URI = "git://git.hugovil.com/git/ympd.git;protocol=http;branch=${SRCBRANCH}"

S = "${WORKDIR}/git"

inherit pkgconfig cmake systemd

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ympd ${D}${bindir}

    install -d ${D}${sysconfdir}/default
    install -m 0644 ${S}/contrib/ympd.default ${D}${sysconfdir}/default/ympd

    install -d ${D}${systemd_unitdir}/system

    sed -i -e 's:^\(Description=.*\):\1\nAfter=mpd.service:g'   ${S}/contrib/ympd.service

    # These options require a kernel compiled with CONFIG_USER_NS:
    sed -i -e 's:^\(PrivateUsers=.*\):#\1:g'   ${S}/contrib/ympd.service
    sed -i -e 's:^\(PrivateTmp=.*\):#\1:g'     ${S}/contrib/ympd.service
    sed -i -e 's:^\(PrivateDevices=.*\):#\1:g' ${S}/contrib/ympd.service
    install -m 0644 ${S}/contrib/ympd.service ${D}${systemd_unitdir}/system
}

FILES:${PN} = " \
    ${systemd_unitdir}/system/ympd.service \
    ${sysconfdir}/default/ympd \
    ${bindir}/ympd \
"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "ympd.service"
