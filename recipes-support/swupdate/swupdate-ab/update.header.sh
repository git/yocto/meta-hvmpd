#!/bin/sh
set -e

# Shell scripts are called via system command. SWUpdate scans for all scripts
# and calls them before and after installing the images. SWUpdate passes
# ‘preinst’ or ‘postinst’ as first argument to the script. If the data attribute
# is defined, its value is passed as the last argument(s) to the script.

trap 'catch $?' EXIT

# Arg1: log message/string
log_info() {
    printf "$(basename ${0}): ${*}\n"
}

log_warn() {
    printf >&2 "$(basename ${0}): ${*}\n"
}

log_err() {
    printf >&2 "$(basename ${0}): ${*}\n"
    exit 1
}

catch()
{
    if [ "$1" != "0" ]; then
        # Error handling goes here
        printf >&2 "$(basename ${0}): Error $1 occurred\n"

        if [ -n "${mnt_point}" ]; then
            if mount | grep -q ${mnt_point}; then
                unmount_destination_partition
            fi
        fi
    fi
}

FSTYPE="ext4"

log_info "arguments = \"${*}\""

if [ $# -lt 2 ]; then
    exit 1;
fi

alias cp=cp

