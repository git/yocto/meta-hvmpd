
# L'environnement U-Boot doit être valide pour utiliser ce script.
# Après la programmation initiale avec uuu, il faut effectuer la sauvegarde
# de l'environnement en mémoire non-volatile (ex: eMMC) dans U-boot avec
# 'saveenv'. Sinon, fw_saveenv va utiliser l'environnement par défaut contenu
# dans /etc/u-boot-initial-env
if fw_printenv 2>&1 | grep -q 'Cannot read environment'; then
    echo "Warning: U-Boot environment cannot be read. Make sure you save the"
    echo "         default environment to flash using these U-Boot commands:"
    echo "             $> env default -a"
    echo "             $> saveenv"
fi

# Return the partition device
get_destination_partition_device()
{
    # Find internal parent kernel device name of rootfs.
    # For example:
    # - /dev/sda1 --> /dev/sda
    # - /dev/mmcblk2p2 --> /dev/mmcblk2
    disk="/dev/$(lsblk -ndo pkname $(findmnt -n -o SOURCE /))"

    if [ ! -b "${disk}" ]; then
        log_err "Error: disk \"${disk}\" not found"
    fi

    update_dev="${disk}${next_part}"

    if [ ! -b "${update_dev}" ]; then
        log_err "Error: destination partition \"${update_dev}\" not found"
    fi

    echo "${update_dev}"
    return 0
}

mount_destination_partition()
{
    update_dev=$(get_destination_partition_device)

    # Create temporary mount point:
    mnt_point=$(mktemp -q -d /tmp/swupdate-mount.XXXXXX)
    if [ ${?} -ne 0 ]; then
        log_err "Error: cannot create temporary file"
    fi

    # Mount update partition:
    mount -t ${FSTYPE} ${update_dev} ${mnt_point}
}

unmount_destination_partition()
{
    if mount | grep -q ${mnt_point}; then
        umount ${mnt_point}
    fi

    if [ -d ${mnt_point} ]; then
        rmdir ${mnt_point}
    fi
}

do_preinst()
{
    update_dev=$(get_destination_partition_device)
    log_info "Reformat destination partition: ${update_dev}"

    label="$(blkid -s LABEL -o value ${update_dev})"

    # Reformat destination partition:
    mkfs.${FSTYPE} ${update_dev} -L "${label}"

    exit 0
}

do_postinst()
{
    mount_destination_partition

    # Perform migration of selected files/folders...
    log_info "migrating existing data..."

    # Migrate files:
    for f in ${SWU_PRESERVE_FILES}; do
        if [ ! -f ${f} ]; then
            log_warn "warning: missing source file: ${f} (skipping)"
            continue
        fi

        dst_folder="$(dirname ${mnt_point}/${f})"

        # Do not copy file if destination folder doesn't exist.
        # The destination folder need to be created in your new SWU archive
        # with the proper ownership and permissions (can be empty)
        if [ ! -d ${dst_folder} ]; then
            log_warn "warning: missing destination folder for file: ${f} (skipping)"
            continue
        fi

        # Copy old file to new partition:
        cp -a ${f} ${mnt_point}/${f}
    done

    # Migrate folders:
    for d in ${SWU_PRESERVE_FOLDERS}; do
        if [ ! -d ${d} ]; then
            log_warn "warning: missing source folder: ${d} (skipping)"
            continue
        fi

        if [ -d ${mnt_point}/${d} ]; then
            # Remove folder if it exists in new partition:
            rm -rf ${mnt_point}/${d}
        fi

        # Copy old folder to new partition:
        cp -a ${d} ${mnt_point}/${d}
    done

    unmount_destination_partition

    exit 0
}

next_part=${2}

case "$1" in
    preinst)
        do_preinst
        ;;
    postinst)
        do_postinst
        ;;
    *)
        log_err "unsupported install mode: \"${1}\""
        ;;
esac
