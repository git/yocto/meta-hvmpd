FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
     file://defconfig \
     file://swupdate.cfg \
     file://99-hvmpd-args.in \
"

do_install:append() {
    install -d ${D}${libdir}/swupdate/conf.d/

    sed -e s/@ROOT_PART_A_ID@/${ROOT_PART_A_ID}/ \
        -e s/@ROOT_PART_B_ID@/${ROOT_PART_B_ID}/ \
        ${WORKDIR}/99-hvmpd-args.in > ${WORKDIR}/99-hvmpd-args
    install -m 644 ${WORKDIR}/99-hvmpd-args ${D}${libdir}/swupdate/conf.d/

    install -m 644 ${WORKDIR}/swupdate.cfg ${D}${sysconfdir}/
}
