SUMMARY = "Deploy SWUpdate shell scripts to support A/B update mechanism"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI = " \
    file://update.header.sh \
    file://update.footer.sh \
"

inherit deploy

# Default list of files/folders to preserve between updates, can be extended/overriden by BSP:
SWU_PRESERVE_FILES ?= "\
    /etc/hostname \
    /etc/localtime \
"
SWU_PRESERVE_FOLDERS ?= ""

DEST_SCR = "${DEPLOYDIR}/${PN}.sh"

do_deploy() {
    cat ${WORKDIR}/update.header.sh > ${DEST_SCR}

    # Insert our variable(s) between header and footer:
    echo "SWU_PRESERVE_FILES=\"\\" >> ${DEST_SCR}
    for f in ${SWU_PRESERVE_FILES}; do
        echo "    ${f} \\" >> ${DEST_SCR}
    done
    echo "\"" >> ${DEST_SCR}

    echo "SWU_PRESERVE_FOLDERS=\"\\" >> ${DEST_SCR}
    for f in ${SWU_PRESERVE_FOLDERS}; do
        echo "    ${f} \\" >> ${DEST_SCR}
    done
    echo "\"" >> ${DEST_SCR}

    cat ${WORKDIR}/update.footer.sh >> ${DEST_SCR}
}

addtask deploy after do_install
