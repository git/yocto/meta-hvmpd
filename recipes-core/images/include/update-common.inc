LICENSE = "CLOSED"

FILESEXTRAPATHS:prepend := "${THISDIR}/update:"

SRC_URI = " \
    file://sw-description \
"

# Image(s) and files that will be included in the .swu image
SWUPDATE_IMAGES = "${IMAGE_DEPENDS} swupdate-ab.sh"

UBOOT_PART_VAR ?= "mmcpart"

inherit swupdate
