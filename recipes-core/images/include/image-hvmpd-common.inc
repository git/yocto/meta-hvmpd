LICENSE = "CLOSED"

inherit core-image

IMAGE_FEATURES += "ssh-server-openssh"

# Common features/components:
IMAGE_INSTALL:append = " \
    libgpiod-tools \
    parted \
    e2fsprogs \
    nano \
    tzdata \
    util-linux \
    sudo \
    evtest \
    i2c-tools \
    ethtool \
    nfs-utils \
"

# Music server specific features/components:
IMAGE_INSTALL:append = " \
    alsa-utils \
    mpd \
    mpc \
    ympd \
    shairport-sync \
"

# Needed by shairport-sync:
IMAGE_INSTALL:append = " \
    avahi-daemon \
"

IMAGE_INSTALL:append = " \
    libubootenv \
    u-boot-fw-utils \
    u-boot-default-env \
"

# Add SWUpdate components
IMAGE_INSTALL:append = " \
    swupdate \
    swupdate-usb \
    swupdate-progress \
"

EXTRA_IMAGEDEPENDS += "swupdate-ab"

# Remove swupdate-www (package with the website, that you can customize with
# your own logo, template and style).
IMAGE_INSTALL:remove = " \
    swupdate-www \
"

IMAGE_FSTYPES += "tar.gz"
