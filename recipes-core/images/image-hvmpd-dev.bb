SUMMARY = "HV MPD development image"

require include/image-hvmpd-common.inc

# Image suitable for development (empty root password):
IMAGE_FEATURES += "debug-tweaks"

# Add debugging tools (gdb and strace, etc):
#IMAGE_FEATURES += "tools-debug"

# Add development tools (gcc, make, pkgconfig, etc):
#IMAGE_FEATURES += "tools-sdk"

# Additional development tools:
IMAGE_INSTALL:append = " \
	curl \
	lsof \
	spitools \
"
