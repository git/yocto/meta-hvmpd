FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
     file://locale.conf \
     file://locale.sh \
"

do_install:append () {
    echo "hvmpd" > ${D}${sysconfdir}/hostname

    # Maybe make this machine-specific in the future...
    echo "${MACHINE} revA" > ${D}${sysconfdir}/hwrevision

    # /etc/locale.conf is systemd-specific.
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/locale.conf ${D}${sysconfdir}

    install -d ${D}${sysconfdir}/profile.d
    install -m 0644 locale.sh ${D}${sysconfdir}/profile.d/

    echo "# Manually added entries for HV MPD server project:" >> \
        ${D}${sysconfdir}/fstab

    # Note: mount points will be automatically created when using systemd.

    if [ x"${BOOT_PART_ID}" != x"" ]; then
        echo "${ROOT_PARENT_DEV}${ROOT_PART_PREFIX}${BOOT_PART_ID}       /boot            auto       defaults              0  0" >> \
            ${D}${sysconfdir}/fstab
    fi

    if [ x"${DATA_PART_ID}" != x"" ]; then
        echo "${DATA_PART_ID}       /mnt/data        auto       defaults              0  0" >> \
            ${D}${sysconfdir}/fstab
    fi

    if [ "${NFS_SERVER}" != "" ]; then
        echo "${NFS_SERVER}:${NFS_SERVER_DIR} ${NFS_MOUNT_POINT}    nfs       bg,nolock   0  0" >> \
            ${D}${sysconfdir}/fstab
    fi
}
